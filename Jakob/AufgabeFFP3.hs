--import qualified Data.List as DL
--import Data.Char
import Prelude hiding (filter)
import qualified Prelude as P(filter)

import qualified Data.List as DL
import qualified Data.Ord as DO

type Weight = Int
type Value = Int
type Item = (Weight, Value)
type Items = [Item]
type Load = [Item]
type Loads = [Load]
type LoadWghtVal = (Load, Weight, Value)
type MaxWeight = Weight

-- Helper functions for accessing LoadWghtVal tuples
ldLoad :: LoadWghtVal -> Load
ldLoad (load, _, _) = load

ldWeight :: LoadWghtVal -> Weight
ldWeight (_, weight, _) = weight

ldValue :: LoadWghtVal -> Value
ldValue (_, _, value) = value

-- list all possible subsequences for a given list (including empty list)
subsequences :: [a] -> [[a]]
subsequences [] = [[]]
subsequences (x:xs) = subsequences xs ++ map (x:) (subsequences xs)

generator :: Items -> Loads
generator = P.filter (not . null) . subsequences

transformer :: Loads -> [LoadWghtVal]
transformer list = zip3 list (map (sum . map fst) list) (map (sum . map snd) list)

filter :: MaxWeight -> [LoadWghtVal] -> [LoadWghtVal]
filter maxw loads = P.filter (\lwv -> ldWeight lwv <= maxw) loads

selector1 :: [LoadWghtVal] -> [LoadWghtVal]
selector1 loads = let
	maxValue = maximum $ map ldValue loads in
	P.filter ((==maxValue) . ldValue) loads

selector2 :: [LoadWghtVal] -> [LoadWghtVal]
selector2 loads = let
	loads' = selector1 loads
	minWeight = minimum $ map ldWeight loads' in
	P.filter ((==minWeight) . ldWeight) $ selector1 loads'

	
binom :: (Integer, Integer) -> Integer
binom (n,k)
	| k==0 || n==k = 1
	| otherwise = binom(n-1, k-1) + binom(n-1, k)
	











