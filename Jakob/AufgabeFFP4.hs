import Data.Array
import Data.Ix

newtype Tbl a b = Tbl (Array a b)

newTable :: (Ix a) => [(a,b)] -> Tbl a b
newTable l = Tbl (array (lo,hi) l) where
	indices = map fst l
	lo = minimum indices
	hi = maximum indices
		
findTable (Tbl a) i = a ! i

updTable p@(i,x) (Tbl a) = Tbl (a // [p])



dynamic :: (Ix coord) => (Tbl coord entry -> coord -> entry) -> (coord, coord) -> (Tbl coord entry)
dynamic compute bnds = t where
	t = newTable (map (\coord -> (coord, compute t coord)) (range bnds))