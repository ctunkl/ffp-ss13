import Data.Array
import Data.List
import Data.Ord

subarrays :: [a] -> [[a]]
subarrays [] = []
subarrays (x:xs) = let
	subs' [] = []
	subs' (x:xs)
		| null xs = [(x:xs)]
		| otherwise = (x:xs) : (subs' xs) in
	subs' (x:xs) ++ subarrays (init (x:xs))

mas :: Array Int Int -> Int
mas = maximum . (map sum) . subarrays . elems

amas :: Array Int Int -> [(Int,Int)]
amas arr = let
	subs' = zip (map (\xs -> (head xs, last xs)) (subarrays (indices arr))) (subarrays (elems arr))
	max = maximum $ map (sum . snd) subs' in
	sort $ map fst (filter ((==max) . sum . snd) subs')

lmas :: Array Int Int -> (Int,Int)
lmas arr = let
	len (a,b) = b - a
	max = maximum $ map len (amas arr) in
	minimum $ filter ((==max) . len) (amas arr)