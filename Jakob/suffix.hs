import Data.Array

getl :: Int -> Int -> String -> Array Int Int -> Int
getl l q p n
	| l > 0 && p !! (l) /= p !! (q-1) = getl (n ! l) q p n
	| otherwise = l

getnext' p q n
	| q > length p = n
	| p !! l' == p !! (q-1) = getnext' p (q+1) (n // [(q,l'+1)])
	| otherwise = getnext' p (q+1) (n // [(q,l')]) where
		l' = getl (n ! (q-1)) q p n
		
getnext p = getnext' p 2 (listArray (1, length p) (repeat 0))
		
suffix' p q next next' suffixes
	| q == length p = suffixes
	| suffixes ! j > q - (next' ! q) = suffix' p (q+1) next next' (suffixes // [(j, q - (next' ! q))])
	| otherwise = suffix' p (q+1) next next' suffixes where
		j = (length p) - (next' ! q)

suffix p = suffix' p 1 next next' (listArray (1, plen) (repeat (plen - (next ! plen)))) where
	next = getnext p
	next' = getnext $ reverse p
	plen = length p