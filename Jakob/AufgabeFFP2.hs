import Data.Char

-- Stream von Primzahlen
-- sieve entfernt bei jedem Durchlauf alle Elemente, die durch das erste Arrayelement (somit immer Primzahl) teilbar sind
primes :: [Integer]
primes = let sieve (x:xs) = x : sieve [x' | x' <- xs, mod x' x > 0] in
    sieve [2..]

-- Stream von Paaren von Primzahlen, die eine Differenz von 2 aufweisen
pps :: [(Integer, Integer)]
pps = let pairs = zip primes $ tail primes in
    [(x,y) | (x,y) <- pairs, y-x == 2]

-- Fakultaet als rekursive Funktion (exponentielle Laufzeit)
pow :: Int -> Integer
pow 0 = 1
pow n = pow (n-1) + pow (n-1)

-- Fakultaet rekursiv mit Memoization Table (lineare Laufzeit)
powList :: [Integer]
powList = [powFast x | x <- [0..]]

powFast :: Int -> Integer
powFast 0 = 1
powFast n = powList !! (n-1) + powList !! (n-1)

fac 0 = 1
fac n = product [1..n]

-- vorgegebene Hilfsfunktion fuer f
h :: (Integral a, Integral b) => a -> b -> Float
h _ 0 = 1
h 0 _ = 0
h z i = let
    z' = fromIntegral z
    i' = fromIntegral i in
    z' ** i' / (fac i')

-- vorgegebene Version von Funktion f
f :: Int -> Int -> Float
f z k = sum $ map (h z) [0..k]

-- Versuch einer Memoization table fuer Funktion f
fMTList :: Int -> [Float]
fMTList z = [fMT z i | i <- [0..]]

-- Version von f mit Memoization Table -- keine Verbesserungen, dafuer muesste z.B. der Wert von z oder i in Hilfsfunktion gecached werden. h allerdings fix vorgegeben, somit hier kein Verbesserungspotenzial
fMT :: Int -> Int -> Float
fMT _ 0 = 1
fMT z k = let z' = fromIntegral z
              k' = fromIntegral k           
              fs = fMTList z !! (k-1)
          in fs + (h z k)

-- liefert Liste von Ziffern als [Int] aus denen Zahl vom Typ Integral besteht
decimals :: Integral a => a -> [Int]
decimals n = map digitToInt $ show $ abs n

-- berechnet Goedelzahl einer Zahl
gz :: Integer -> Integer
gz n
    | n > 0 = product $ zipWith (^) primes $ decimals n
    | otherwise = 0

-- liefert Stream aller Goedelzahlen
gzs :: [Integer]
gzs = map gz [1..]
