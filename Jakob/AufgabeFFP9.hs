import Data.List
import Data.Array hiding (indices)
import Data.Maybe
import Data.Map (Map)
import qualified Data.Map as M hiding (Map)
import Data.Ord
import Test.QuickCheck

type Text = String
type Word = String
type First = Int
type Last = Int


len :: Num i => [a] -> i
len = genericLength


occ :: Text -> Word -> [(First,Last)]
occ = occI

occS :: Text -> Word -> [(First,Last)]
occS = occ_naive 0


-- len (genericTake lw text) to avoid expensive processing of the full text
occ_naive :: Int -> Text -> Word -> [(First, Last)]
occ_naive i text "" = []
occ_naive i text word
  | lw > len (genericTake lw text) = []
  | matched = (indices i lw) : (rest lw)
  | otherwise = rest 1
  where lw =  len word
        matched = startsWith text word
        rest skip = occ_naive (i+skip) (genericDrop skip text) word



-- The word length is used a lot, so we'll just calculate it once here
-- skipFunctions contains functions of the form Int -> Text -> Word -> Int (MismatchIndex -> Text -> Word -> PositionsToSkip)
occI :: Text -> Word -> [(First,Last)]
occI text word = occ_bm 0 text word (len word) skipFunctions
  where badTab = lastIndices word
        goodTab = goodSuffixTable word
        skipFunctions = [badCharacterSkip badTab, goodCharacterSkip goodTab]
        --skipFunctions = [badCharacterSkip badTab]
        --skipFunctions = [goodCharacterSkip goodTab]

occ_bm i text _ 0 fs = [] 
occ_bm i text word lw fs
  | lw > len (genericTake lw text) = []
  | index < 0 = (indices i lw) : rest
  | otherwise = rest
  where index = match i (genericTake lw text) word
        mSkip = maxSkip index text word fs
        skip = if index < 0 then lw else mSkip
        rest = occ_bm (i+skip) (genericDrop skip text) word lw fs
                     

-- Returns the index of the first mismatched character from the right, or (-1) if match
match :: Int -> Text -> Word -> Int
match i t w =  let rt = reverse t
                   rw = reverse w
                   matchedCount = len $ zipWhile (==) rt rw                    
               in (len w - matchedCount) - 1                                    


-- Bad Character Table

badCharacterSkip :: Map Int (Map Char Int) -> Int -> Text -> Word -> Int
badCharacterSkip badTab i text word
  | skip < 0 = (i+1)
  | otherwise = i - skip
  where skip = tableLookup badTab (text !! i) i

tableLookup :: Map Int (Map Char Int) -> Char -> Int -> Int
tableLookup m c i = fromMaybe (-1) $ return m >>= (M.lookup i) >>= (M.lookup c)

-- Calculates the last indices of each character for each prefix of the word.
-- Folds the word using a map, and stores each iteration of the map in a list
lastIndices :: Word -> Map Int (Map Char Int)
lastIndices = M.fromList . zipWith (,) [1..] . snd . foldl (\((i, m), xs) c -> let next@(i', m') = (i+1, M.insert c i m) in (next, xs ++ [m'])) ((0, M.empty), [])


-- Good Suffix Table

goodCharacterSkip :: Map Int Int -> Int -> Text -> Word -> Int
goodCharacterSkip goodTab i text word = fromJust $ M.lookup (i + 1) goodTab



-----
getl :: Int -> Int -> String -> Array Int Int -> Int
getl l q p n
	| l > 0 && p !! (l) /= p !! (q-1) = getl (n ! l) q p n
	| otherwise = l

getnext' :: Word -> Int -> Array Int Int -> Array Int Int
getnext' p q n
	| q > length p = n
	| p !! l' == p !! (q-1) = getnext' p (q+1) (n // [(q,l'+1)])
	| otherwise = getnext' p (q+1) (n // [(q,l')]) where
		l' = getl (n ! (q-1)) q p n
		
getnext :: Word -> Array Int Int
getnext p = getnext' p 2 (listArray (1, length p) (repeat 0))

suffix' :: Word -> Int -> Array Int Int -> Array Int Int -> Array Int Int -> Array Int Int
suffix' p q next next' suffixes
	| q == length p = suffixes
	| suffixes ! j > q - (next' ! q) = suffix' p (q+1) next next' (suffixes // [(j, q - (next' ! q))])
	| otherwise = suffix' p (q+1) next next' suffixes where
		j = (length p) - (next' ! q)

suffix :: Word -> Array Int Int
suffix p = suffix' p 1 next next' (listArray (1, plen) (repeat (plen - (next ! plen)))) where
	next = getnext p
	next' = getnext $ reverse p
	plen = length p
-----

goodSuffixTable :: Word -> Map Int Int
--goodSuffixTable w = M.fromList $ map (\i -> (i, goodSkipValue w i)) [0..(len w - 1)]
goodSuffixTable w = M.fromList $ assocs $ suffix w


goodSkipValue w i
  | suff /= (-1) = (len w) - suff
  | otherwise = (len w) - pref
  where suff = findSuffix w i
        pref = findPrefix w i

findSuffix :: Word -> Int -> Int
findSuffix w i = let lw = len w
                     suffix = genericDrop (i+1) w
                     prefs = prefixes w
                     biggestPrefixIndex = filter (checkPrefix w i suffix prefs) $ findIndices (\pref -> endsWith pref suffix) prefs
                 in case biggestPrefixIndex of
                    [] -> (-1)
                    xs -> maximum xs                    


checkPrefix w i suffix prefs prefixIndex = let pref = prefs !! prefixIndex
                                               cw = w !! i
                                               pp = genericTake (len pref - len suffix) pref
                                           in case pp of
                                              [] -> False
                                              cp -> cw /= (last cp)

suffixes :: String -> [String]
suffixes s = let tails' = tails s
             in if len tails' > 1 then init $ tail tails' else []

prefixes :: String -> [String]
prefixes s = let inits' = inits s
             in if len inits' > 1 then init $ tail inits' else []

--findSuffix :: Word -> Int -> Int
--findSuffix w i = let n = len w
--                     suff = drop i w
--                     wInits = init $ tail $ reverse $ inits w
--                 in case find (\ini -> endsWith ini suff) wInits of
--                    Nothing -> (-1)
--                    Just x -> if last (take (len x - len suff) x) /= (w !! (i-1)) then len x else (-1)

findPrefix :: Word -> Int -> Int
findPrefix w i = let str = genericDrop i w
                     suffs  = init $ tail $ tails str
                 in case find (\suff -> startsWith w suff) suffs of
                    Nothing -> (-1)
                    Just x -> len x




-- Tools

startsWith :: String -> String -> Bool
startsWith x y = let ly = len y in (genericTake ly x) == y

endsWith :: String -> String -> Bool
endsWith x y = let lx = len x 
                   ly = len y 
               in (genericDrop (lx-ly) x) == y


-- Applies all skipFunctions to the input, returns the biggest result
maxSkip i text word fs = maximum  $ map (\f -> f i text word) fs

indices :: Int -> Int -> (First, Last)
indices i lw = (i, i + lw-1)

zipWhile c xs ys = takeWhile (\(x,y) -> c x y) $ zip xs ys




-- Testing


prop_coincide :: Text -> Word -> Bool
prop_coincide text word = (occS text word) == (occI text word)


