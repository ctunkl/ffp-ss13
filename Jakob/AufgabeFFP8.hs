import Data.Maybe

type TargetValue = Integer
type Digit = Integer
type Digits = [Digit]

-- available digits
digits = [1..9] :: Digits

data Operator = P | T deriving (Eq, Show)
data Expr = Opd Digit | Opr Operator Expr Expr deriving (Eq, Show)

-- evaluate expression tree
eval :: Expr -> Integer
eval (Opd val) = val
eval (Opr P expr1 expr2) = (eval expr1) + (eval expr2)
eval (Opr T expr1 expr2) = (eval expr1) * (eval expr2)


parts_naive :: Integer -> [Expr] -> [[[Expr]]]
parts_naive _ [] = [[[]]]
parts_naive target (x:[]) = [[[x]]]
parts_naive target (x:xs) = map (\p -> ((x : (head p)) : (tail p))) parts' ++ map (\p -> [x] : (p)) parts' where
		parts' = parts_naive target xs

parts :: Integer -> (Expr -> Expr -> Expr) -> [Expr] -> [[Expr]]
parts _ _ [] = [[]]
parts target _ (x:[]) = [[x]]
parts target op (x:xs) = mapMaybe (\p -> joinExprs p x) parts' ++ map (\p -> x : p) parts' where
	joinExprs exprs expr
		| null exprs = Just [expr]
		| eval (op expr (head exprs)) > target = Nothing
		| otherwise = Just ((op expr (head exprs)) : (tail exprs))
	parts' = parts target op xs
	
-- concat two expressions that contain plain Digits (i.e. no operations with multiple operands) as decimal strings
-- e.g. (Opd 12), (Opd 34) -> 1234
decconcat :: Expr -> Expr -> Expr
decconcat (Opd a) (Opd b) = Opd (read ((show a) ++ (show b)))
	
-- Expand list of possible partitions using the given operation.
expand :: [[[Expr]]] -> (Expr -> Expr -> Expr) -> [[Expr]]
expand ps op = map (map (foldr1 op)) ps
	
-- create all possible combinations of operation trees
mkAll digits target = let
	exprs = parts_naive target (map Opd digits)
	solutions = map (foldr1 (Opr P)) (expand (concat $ map (parts_naive target) (expand exprs decconcat)) (Opr T)) in
	solutions

-- return all operation trees that evaluate to target
mkTV_naive :: Digits -> TargetValue -> [Expr]
mkTV_naive digits target = let
	exprs = parts_naive target (map Opd digits)
	solutions = map (foldr1 (Opr P)) (expand (concat $ map (parts_naive target) (expand exprs decconcat)) (Opr T)) in
	filter ((==target) . eval) solutions

-- return all operation trees that evaluate to target (optimized version)	
mkTV :: Digits -> TargetValue -> [Expr]
mkTV digits target = let
	digitExprs = map Opd digits
	solutions = map (foldr1 (Opr P)) $ concat (map (parts target (Opr T)) (parts target (decconcat) digitExprs)) in
	filter ((==target) . eval) solutions