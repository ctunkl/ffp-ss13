{-# LANGUAGE FlexibleInstances #-}

import Data.Array
import Data.List
import Data.Maybe

import Control.Monad

instance Show (Int -> Int -> Int) where
	show op = showOp op

instance Eq (Int -> Int -> Int) where
	op1 == op2 = (showOp op1) == (showOp op2)
	
showOp :: (Int -> Int -> Int) -> String
showOp op
	| op 6 2 == 8 = "plus"
	| op 6 2 == 4 = "minus"
	| op 6 2 == 12 = "times"
	| op 6 2 == 3 = "div"
	| otherwise = "undefined"

eval :: Array Int Int -> Array Int (Int -> Int -> Int) -> Int
eval nums ops = let
	nums' = elems nums
	ops' = elems ops in
	foldl (\x y -> (snd y) x (fst y)) (head nums') (zip (tail nums') ops')

	

yield :: Array Int Int -> Int -> [Array Int (Int -> Int -> Int)]
yield arr sol = let 
	bnds = bounds arr
	arrlen = (snd bnds) - (fst bnds) + 1
	sol_arr = replicateM sol [(+), (-)]
	num_sols = length sol_arr in
	map (\sol' -> array (1, length sol') (zip [1..(length sol')] sol')) sol_arr
	-- TODO: filter
	
	
--

data Stack a = EmptyStk | Stk a (Stack a)

push x s = Stk x s

pop EmptyStk = error "pop from an empty stack"
pop (Stk _ s) = s

top EmptyStk = error "top from an empty stack"
top (Stk x _) = x

emptyStack = EmptyStk

stackEmpty EmptyStk = True
stackEmpty _ = False

(./.) :: Int -> Int -> Int
(./.) = div


nextOp :: (Int -> Int -> Int) -> Maybe (Int -> Int -> Int)
nextOp op
	| show op == "plus" = Just (-)
	| show op == "minus" = Just (*)
	| show op == "times" = Just (./.)
	| show op == "div" = Nothing

searchDfs :: (Eq node) => (node -> [node]) -> (node -> Bool) -> node -> [node]
searchDfs succ goal x = search' (push x emptyStack) where
	search' s
		| stackEmpty s = []
		| goal (top s) = top s : search' (pop s)
		| otherwise = search' (foldr push (pop s) (succ (top s)))

succYield :: Array Int (Int -> Int -> Int) -> [Array Int (Int -> Int -> Int)]
succYield node = let
	succ' n
		| isNothing (nextOp (node!n)) = Nothing
		| otherwise = Just (node//[(n, fromJust (nextOp (node!n)))]) in
	mapMaybe succ' [1..(length (elems node))]

yield_bt :: Array Int Int -> Int -> [Array Int (Int -> Int -> Int)]
yield_bt nums target = let
	len_nums = length (elems nums)
	succ = succYield
	goal node = eval nums node == target in
	searchDfs succ goal (array (1, len_nums - 1) (zip [1..(len_nums-1)] (replicate (len_nums-1) (+))))








