import Data.List hiding (delete, insert)
import Data.Char
import Test.QuickCheck


instance Arbitrary Char where
    arbitrary     = choose ('a', 'z')
    coarbitrary c = variant (ord c `rem` 4)



-- Buffer

type Buffer = (Int,String)

empty :: Buffer -- the empty buffer
empty = (0, "")

insert :: Char -> Buffer -> Buffer -- insert character before cursor
insert c (cur, s) = let (s1, s2) = splitAt cur s
                    in (cur+1, s1 ++ [c] ++ s2)

delete :: Buffer -> Buffer -- delete character before cursor
delete (0, s) = (0, s)
delete (cur, s) = (cur-1, removeIndex (cur-1) s)

left :: Buffer -> Buffer -- move cursor left one character
left (0, s) = (0, s)
left (cur, s) = (cur-1, s)

right :: Buffer -> Buffer -- move cursor right one character
right (cur, s) = (min (cur+1) (len s), s)

atLeft :: Buffer -> Bool -- is cursor at left end?
atLeft (cur, _) = cur == 0

atRight :: Buffer -> Bool -- is cursor at right end?
atRight (cur, s) = cur == len s


-- BufferI

type BufferI = (String,String)

emptyI :: BufferI -- the empty BufferI
emptyI = ("", "")

insertI :: Char -> BufferI -> BufferI -- insert character before cursor
insertI c (xs, ys) = (c:xs, ys)

deleteI :: BufferI -> BufferI -- delete character before cursor
deleteI ("", ys) = ("", ys)
deleteI (x:xs, ys) = (xs, ys)

leftI :: BufferI -> BufferI -- move cursor left one character
leftI ("", ys) = ("", ys)
leftI (x:xs, ys) = (xs, x:ys)

rightI :: BufferI -> BufferI -- move cursor right one character
rightI (xs, "") = (xs, "")
rightI (xs, y:ys) = (y:xs, ys)

atLeftI :: BufferI -> Bool -- is cursor at left end?
atLeftI ("", _) = True
atLeftI _ = False

atRightI :: BufferI -> Bool -- is cursor at right end?
atRightI (_, "") = True
atRightI _ = False


retrieve :: BufferI -> Buffer
retrieve (s1, s2) = (len s1, (reverse s1) ++ s2)


-- Testing

prop_empty = retrieve emptyI == empty
prop_insert c bi = retrieve (insertI c bi) == insert c (retrieve bi)
prop_delete bi = retrieve (deleteI bi) == delete (retrieve bi)
prop_left bi = retrieve (leftI bi) == left (retrieve bi)
prop_right bi = retrieve (rightI bi) == right (retrieve bi)
prop_atLeft bi = atLeftI bi == atLeft (retrieve bi)
prop_atRight bi = atRightI bi == atRight (retrieve bi)

prop_inv_left bi = atLeftI bi ==> atLeftI (leftI bi)
prop_inv_right_left bi = not (atLeftI bi) ==> (rightI.leftI) bi == bi

prop_inv_right bi = atRightI bi ==> atRightI (rightI bi)
prop_inv_left_right bi = not (atRightI bi) ==> (leftI.rightI) bi == bi

prop_insert_delete c bi = deleteI (insertI c bi) == bi
prop_empty_insert_delete c = deleteI (insertI c emptyI) == emptyI




-- Tools

len :: Num i => [a] -> i
len = genericLength

removeIndex :: Integral i => i -> [a] -> [a]
removeIndex i list = (genericTake i list) ++ (genericDrop (i+1) list)