import Data.Array
import Data.List
import Data.Ord

data Color = Black | White deriving Show
data Digit = One | Two | Three | Four | Five | Six | Seven | Eight | Nine | Blank deriving Show
type Str8ts = Array (Int,Int) (Color,Digit)
type ColorOut = Char -- Nur ’b’ fuer schwarz und ’w’ fuer weiss werden benutzt.
type DigitOut = Int -- Nur 1,2,3,4,5,6,7,8,9,0 werden benutzt; 0 wird dabei als Platzhalter fuer ’blank’ benutzt.
type Str8tsOut = Array (Int,Int) (ColorOut,DigitOut)

type Cell = (Color,Integer)
type Row = [Cell]
type Column = [Cell]
type Grid = [Row]

type Choices = (Color, [Integer])

size :: Num a => a
size = 9


naiveStr8ts :: Str8ts -> Str8tsOut
naiveStr8ts = solveStr8ts slowSolve

fastStr8ts :: Str8ts -> Str8tsOut
fastStr8ts = solveStr8ts fastSolve

-- Unwraps the input array, converts the custom type Digit to an Integer and calls the solving method.
-- Solutions are converted to the output type and again wrapped into an array.
-- If there's no solution, the input is simply converted to the output type.
solveStr8ts :: ([Cell] -> Maybe Grid) -> Str8ts -> Str8tsOut
solveStr8ts f input = let result = f $ digitsToNumbers $ elems input
                          op = gridToArray  . colorsToLetters
                      in case result of
                            Nothing -> str8tsInToOut input
                            Just solution -> op solution

slowSolve :: [Cell] -> Maybe Grid
slowSolve = solve . splitIntoGrids . cp . choices

fastSolve :: [Cell] -> Maybe Grid
fastSolve = solve . chunksOf size . expand . prune . chunksOf size . choices


str8tsInToOut :: Str8ts -> Str8tsOut
str8tsInToOut input = gridToArray $ chunksOf size $ map (\(c,d) -> (toLetter c, toNum d)) (elems input)

digitsToNumbers = map (\(c,d) -> (c, toNum d))


-- Converts the final result to the expected St8tsOut type
gridToArray :: [[(Char,Integer)]] -> Str8tsOut
gridToArray xs = let list = concat [[get xs i j | j <- [1..size]] | i <- [1..size]]
                 in array ((1,1),(size,size)) list

get xs i j = let (c,n) = (xs !! (i-1)) !! (j-1)
             in ((i,j),(c, fromInteger n))


colorsToLetters xs = map (map (\(c,d) -> (toLetter c, d))) xs



-- choices/choice: Calculates all possible numbers for each cell
choices :: [Cell] -> [Choices]
choices = map choice

choice :: Cell -> Choices
choice (White, 0) = (White, [1..size])
choice (c, d) = (c, [d])

-- Single-cell-expansion: One cell is expanded, then the whole grid is pruned. The resulting grid is expanded recursively. This prevents duplicates in the solutions.
expand :: [[Choices]] -> [[Cell]]
expand xs
  | hasChoices xs = (concat . map expand . map prune . expand1) xs
  | otherwise = map flattenChoices xs

hasChoices = any (>1) . concatMap (map (length . snd))

flattenChoices :: [Choices] -> Row
flattenChoices = map flattenChoice

-- There's an error in expand/prune which results in empty choice-lists. Couldn't find it.
-- This ugly workaround is to detect solutions with empty lists (see isFull function)
flattenChoice (c,[d]) = (c,d)
flattenChoice (c,[]) = (c,99)

-- Taken from the slides and adapted. Single-cell-expansion
expand1 :: [[Choices]] -> [[[Choices]]]
expand1 rows = if null (counts rows) then [rows] else [rows1 ++ [row1 ++ (c,[x]) : row2] ++ rows2 | x<-cs]
  where (rows1,row:rows2) = break (any smallest) rows
        (row1, (c,cs):row2) = break smallest row
        smallest (c,cs) = length cs == n
        n = minimum (counts rows)
        counts = filter (/=1) . map (length . snd) . concat


-- Solutions given as list of cells are converted to a list of grids (= list of list of cells)
splitIntoGrids :: [[Cell]] -> [Grid]
splitIntoGrids = map (chunksOf size)

-- If some cell has a fixed value, prune removes it from the other cells in the same row and column
prune :: [[Choices]] -> [[Choices]]
prune = transpose . map pruneRow . transpose . map pruneRow

pruneRow :: [Choices] -> [Choices] 
pruneRow row = map (remove fixed) row
               where fixed = [d | (c,[d]) <- row]

-- Removes values already fixed in other cells
remove :: [Integer] -> Choices -> Choices
remove fixed cell@(c,[d]) = cell
remove fixed cell@(c,d) = (c, d \\ fixed)

-- Takes a list of solutions and removes invalid ones. Returns the first valid solution, if there is one.
solve :: [Grid] -> Maybe Grid
solve grids = let fullGrids = filter isFull grids 
                  validGrids = filter isValid fullGrids
              in case validGrids of 
                      [] -> Nothing
                      (x:xs) -> Just x

-- Indicates solutions with empty cells. See flattenChoice
noneTrue = not . or
isFull = noneTrue . concatMap (map ((==99) . snd))

-- A grid is valid if all rows as valid and all columns are valid
isValid :: Grid -> Bool
isValid grid = all listValid (grid) && all listValid (transpose grid)

-- A list of cells os valid if there are no duplicates and every range of connected white cells is a segment
listValid :: Row -> Bool
listValid row = onlySegments row && noDups row

noDups :: Row -> Bool
noDups [] = True
noDups (x@(c,d):xs)
  | elem x xs && d /= 0 = False
  | otherwise = noDups xs


-- Extracts the cell values and checks if they are segments
onlySegments :: Row -> Bool
onlySegments row = and $ map isSegment $ map (map snd) $ splitByBlackCells row


splitByBlackCells :: [Cell] -> [[Cell]]
splitByBlackCells [] = [[]]
splitByBlackCells xs = let (first, rest) = break isBlack xs in first : splitByBlackCells (dropBlacks rest)

-- The second element of "break isBlack" starts with a black cell, possibly several. Drops them
dropBlacks [] = []
dropBlacks ((Black,_):xs) = dropBlacks xs
dropBlacks xs = xs

isBlack :: Cell -> Bool
isBlack (Black,_) = True
isBlack _ = False

-- Any segment from a to b must have the length (b-a+1)
isSegment :: [Integer] -> Bool
isSegment [] = True
isSegment xs = (maximum xs - minimum xs + 1) == genericLength xs

-- Taken from the slides and adapted. Calculates all possible choices (incl duplicates)
cp :: [Choices] -> Grid
cp [] = []
cp [(c,digits)] = map (\d -> [(c,d)]) digits
cp ((c,digits):xss) = [(c,d):ys| d <- digits, ys <- cp xss]


{-test1 :: Str8ts
test9 = array ((1,1), (9,9)) [((1,1),(Black,Blank)),((1,2),(Black,Blank)),((1,3),(White,Blank)),((1,4),(White,Blank)),((1,5),(White,Blank)),((1,6),(White,Blank)),((1,7),(White,Blank)),((1,8),(Black,Three)),((1,9),(Black,Blank)),
         ((2,1),(White,Eight)),((2,2),(White,Six)),((2,3),(White,Five)),((2,4),(White,Seven)),((2,5),(Black,Blank)),((2,6),(Black,Blank)),((2,7),(White,One)),((2,8),(White,Two)),((2,9),(White,Three)),
         ((3,1),(White,Seven)),((3,2),(White,Five)),((3,3),(White,Six)),((3,4),(Black,Blank)),((3,5),(Black,Eight)),((3,6),(White,Two)),((3,7),(White,Three)),((3,8),(White,One)),((3,9),(White,Four)),
         ((4,1),(White,Nine)),((4,2),(White,Eight)),((4,3),(White,Seven)),((4,4),(Black,Four)),((4,5),(White,Blank)),((4,6),(White,Blank)),((4,7),(White,Blank)),((4,8),(Black,Blank)),((4,9),(Black,Five)),
         ((5,1),(Black,Blank)),((5,2),(White,Seven)),((5,3),(White,Four)),((5,4),(White,One)),((5,5),(White,Two)),((5,6),(White,Three)),((5,7),(White,Six)),((5,8),(White,Five)),((5,9),(Black,Blank)),
         ((6,1),(Black,Blank)),((6,2),(Black,Blank)),((6,3),(White,Two)),((6,4),(White,Three)),((6,5),(White,One)),((6,6),(Black,Nine)),((6,7),(White,Five)),((6,8),(White,Four)),((6,9),(White,Six)),
         ((7,1),(White,Four)),((7,2),(White,One)),((7,3),(White,Three)),((7,4),(White,Two)),((7,5),(Black,Blank)),((7,6),(Black,Blank)),((7,7),(White,Eight)),((7,8),(White,Six)),((7,9),(White,Seven)),
         ((8,1),(White,Three)),((8,2),(White,Two)),((8,3),(White,One)),((8,4),(Black,Blank)),((8,5),(Black,Blank)),((8,6),(White,Six)),((8,7),(White,Nine)),((8,8),(White,Seven)),((8,9),(White,Eight)),
         ((9,1),(Black,Blank)),((9,2),(Black,Blank)),((9,3),(White,Blank)),((9,4),(White,Blank)),((9,5),(White,Blank)),((9,6),(White,Blank)),((9,7),(White,Blank)),((9,8),(Black,Blank)),((9,9),(Black,Two))]-}

{-test4 :: Str8ts
test4 = array ((1,1),(4,4)) [((1,1),(White,Blank)),((1,2),(White,Two)),((1,3),(White,Blank)),((1,4),(White,Blank)),
              ((2,1),(White,Blank)),((2,2),(White,Blank)),((2,3),(Black,Blank)),((2,4),(Black,Two)), 
              ((3,1),(White,Blank)),((3,2),(Black,Blank)),((3,3),(White,Blank)),((3,4),(White,Blank)),
              ((4,1),(White,Blank)),((4,2),(White,Blank)),((4,3),(White,Blank)),((4,4),(White,Blank))]-}


toLetter Black = 'b'
toLetter White = 'w'

-- Just in case that using "deriving Eq" would break the automated testing
instance Eq Color where
  Black == Black = True
  White == White = True
  _ == _ = False

toNum One = 1
toNum Two = 2
toNum Three = 3
toNum Four = 4
toNum Five = 5
toNum Six = 6
toNum Seven = 7
toNum Eight = 8
toNum Nine = 9
toNum Blank = 0


-- Splits a list into a list of sublists with a specified size
chunksOf :: Int -> [e] -> [[e]]
chunksOf _ [] = [[]]
chunksOf _ [x] = [[x]]
chunksOf i xs
  | null (drop i xs) = [take i xs]
  | otherwise = (take i xs) : chunksOf i (drop i xs)
