import Data.Char

primes = sieve [2..]
sieve (x:xs) = x : sieve [y |  y <- xs, mod y x > 0]

pps :: [(Integer, Integer)]
pps = filter (\(x,y) -> (y-x) == 2) $ zip primes (tail primes)


pow :: Int -> Integer
pow 0 = 1
pow n = pow (n-1) + pow (n-1)

powList :: [Integer]
powList = [powFast x | x <- [0..]]

powFast :: Int -> Integer
powFast 0 = 1
powFast n = powList !! (n-1) + powList !! (n-1)



h :: Int -> Int -> Float
h z i = let z' = fromIntegral z
            i' = fromIntegral i 
        in (z' ** i') / (fac i')


fac 0 = 1
fac n = foldr1 (*) [1..n]

f :: Int -> Int -> Float
f z k  = sum $ map (h z) [0..k]


fMTList :: Int -> [Float]
fMTList z = [fMT z i | i <- [0..]]

fMT :: Int -> Int -> Float
fMT _ 0 = 1
fMT z k = let z' = fromIntegral z
              k' = fromIntegral k           
              fs = fMTList z !! (k-1)
          in fs + (h z k)


decimals n = map digitToInt $ show n

gz :: Integer -> Integer
gz n = product $ zipWith (^) primes (decimals n)

gzs :: [Integer]
gzs = map gz [1..]
