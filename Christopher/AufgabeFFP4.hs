import Data.List ((\\), maximumBy)
import Data.Ord
import Data.Array
import Data.Ix

type Weight = Int -- Gewicht
type Value = Int -- Wert
type MaxWeight = Weight -- Hoechstzulaessiges Rucksackgewicht
type Object = (Weight,Value) -- Gegenstand als Gewichts-/Wertpaar
type Objects = [Object] -- Menge der anfaenglich gegebenen Gegenstaende
type SolKnp = [Object] -- Auswahl aus der Menge der anfaenglich gegebenen Gegenstaende; 
                       --moegliche Rucksackbeladung, falls zulaessig

type NodeKnp = (Value,Weight,MaxWeight,[Object],SolKnp)


totalWeight :: [(Weight, b)] -> Weight
totalWeight = sum . map fst

totalValue :: [(a, Value)] -> Value
totalValue = sum . map snd

succKnp :: NodeKnp -> [NodeKnp]
succKnp (v,w,limit,objects,psol) = let nodeSelections = filter (not. null) (subsequences objects)                                                                              
                                       f = \x -> let newValue = v + totalValue x
                                                     newWeight = w + totalWeight x
                                                     newObjects = objects \\ x
                                                     newPsol = psol ++ x
                                                 in  (newValue, newWeight, limit, newObjects, newPsol)
                                   in map f nodeSelections

value :: NodeKnp -> Value
value (v,_,_,_,_) = v

goalKnp :: NodeKnp -> Bool
goalKnp (_,w,limit,_,_) = w <= limit
--goalKnp (_,w,limit,((w',_):_),_) = w <= limit

knapsack :: Objects -> MaxWeight -> (SolKnp,Value)
knapsack objects limit = let root = (0,0,limit,objects,[])
                             solutions = searchDfs root
                             (v,_,_,_,selectedObjects) = maximumBy (comparing value) solutions
                         in (selectedObjects, v)

searchDfs :: NodeKnp -> [NodeKnp]
searchDfs node
    | goalKnp node = node : concatMap searchDfs (succKnp node)
    | otherwise = []


-- Aufgabe 2


newtype Table a b = Tbl (Array b a)

newTable :: (Ix b) => [(b,a)] -> Table a b
newTable l = Tbl (array (lo,hi) l)
  where indices = map fst l
        lo = minimum indices
        hi = maximum indices

findTable :: (Ix b) => Table a b -> b -> a
findTable (Tbl a) i = a ! i

bndsB :: (Integer, Integer) -> ((Integer, Integer), (Integer, Integer))
bndsB (n,k) = ((0,0), (n,k))


compB :: Table Integer (Integer, Integer) -> (Integer, Integer) -> Integer
compB t (n, k)
  | k == 0 || n == k = 1
  | k < 0 || k > n = 0
  | otherwise = findTable t (n-1, k-1) + findTable t (n-1, k)

binomDyn :: (Integer, Integer) -> Integer
binomDyn (n,k) = findTable t (n,k)
  where t = dynamic compB (bndsB (n,k))


dynamic :: (Ix coord) => (Table entry coord -> coord -> entry) -> (coord,coord) -> (Table entry coord)
dynamic compute bnds = t
   where t = newTable (map (\coord -> (coord,compute t coord)) (range bnds))




-- Hilfsfunktionen


subsequences :: [a] -> [[a]]
subsequences [] = [[]]
subsequences (x:xs) = subsequences xs ++ map (x:) (subsequences xs)
