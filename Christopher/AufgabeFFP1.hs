import Data.List

-- Recursively applies (2*) to the start value 1, yielding all powers of two. Equivalent to iterate (2*) 1.
pof2s :: [Integer]
pof2s = 1 : map (2*) pof2s


--Starting with [1], calculates all intermediate sums of a row of the triangle by zipping the row with itself shifted by one. The sums are then embedded between two [1]'s.
pd :: [[Integer]]
pd = [1] : [1 : zipWith (+) p (tail p) ++ [1]  | p <- pd]


--As the latest needed row is the nth row, the first n rows are taken and reversed. From the remaining rows, those which are not needed are discarded as well ([0..(n-1)/2]). From the remaining list of rows, the xth elements are taken where x is the index of the row.
fibdiag :: Integer -> [Integer]
fibdiag n = zipWith (\x p -> p !! x) [0..(div (fromInteger n-1) 2)] (reverse $ genericTake n pd)


fibdiags :: [[Integer]]
fibdiags = map fibdiag [1..]


fibspd :: [Integer]
fibspd = map sum fibdiags

