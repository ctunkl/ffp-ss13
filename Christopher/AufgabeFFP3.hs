import qualified Data.List as List
import Data.Ord



-- Aufgabe 1

type Weight = Int -- Gewicht
type Value = Int -- Wert
type Item = (Weight,Value) -- Gegenstand als Gewichts-/Wertpaar
type Items = [Item] -- Menge der anfaenglich gegebenen Gegenstaende
type Load = [Item] -- Auswahl aus der Menge der anfaenglich gegebenen Gegenstaende; moegliche Rucksackbeladung, falls zulaessig
type Loads = [Load] -- Menge moeglicher Auswahlen
type LoadWghtVal = (Load,Weight,Value) -- Eine moegliche Auswahl mit Gesamtgewicht/-wert dieser Auswahl
type MaxWeight = Weight -- Hoechstzulaessiges Rucksackgewicht

generator :: Items -> Loads
generator = List.filter (not . null) . subsequences

transformer :: Loads -> [LoadWghtVal]
transformer [] = []
transformer (x:xs) = let
                       totalWeight = sum $ map fst x
                       totalValue  = sum $ map snd x
                     in 
                       (x, totalWeight, totalValue) : transformer xs

filter :: MaxWeight -> [LoadWghtVal] -> [LoadWghtVal]
filter mw = List.filter (\item -> (snd3 item) <= mw)


selector1 :: [LoadWghtVal] -> [LoadWghtVal]
selector1 loads = let maxVal = thd3 $ List.maximumBy (comparing thd3) loads
                  in List.filter ((maxVal==) . thd3) loads

selector2 :: [LoadWghtVal] -> [LoadWghtVal]
selector2 loads = let sel1Loads = selector1 loads
                      minWeight = snd3 $ List.minimumBy (comparing snd3) sel1Loads
                  in List.filter ((minWeight==) . snd3) sel1Loads


--Aufgabe 2

binom :: (Integer,Integer) -> Integer
binom (n,k)
  | k==0 || n==k = 1
  | otherwise = binom (n-1,k-1) + binom (n-1,k)


--Stream

pd :: [[Integer]]
pd = [1] : [1 : zipWith (+) p (tail p) ++ [1]  | p <- pd]

binomS :: (Integer,Integer) -> Integer
binomS (n,k) = let  k'  = fromInteger k
                    n'  = fromInteger n
                    pd' = init pd                    
               in pd' !! n' !! k'



--Memoization

binomM :: (Integer,Integer) -> Integer
binomM (n,k)
  | k==0 || n==k = 1
  | otherwise = let k' = fromInteger k
                    n' = fromInteger n
                    binList = binomList !! (n'-1)               
                in  binList !! (k'-1) + binList !! k'

binomList  = [[binomM (n,k) | k <- [0..n]] | n <- [0..]]



--Hilfsfunktionen

subsequences :: [a] -> [[a]]
subsequences [] = [[]]
subsequences (x:xs) = subsequences xs ++ map (x:) (subsequences xs)

fst3 :: (a,b,c) -> a
fst3 (x,_,_) = x

snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x

thd3 :: (a,b,c) -> c
thd3 (_,_,x) = x
