{-# LANGUAGE FlexibleInstances #-}

import Control.Monad
import Data.List
import Data.Array
import Numeric
import Data.Char

type ArithOp = (Int -> Int -> Int)
type OpSeq = [ArithOp]


(./.) :: Int -> Int -> Int
(./.) = div

-- Shorter name.
len :: Num i => [b] -> i
len = genericLength

-- Basic operations. Can be easily extended, e.g. power, modulo, ...
baseOps = [(+), (-), (*), (./.)]

-- All possible combinations of n operators
opseqs n = replicateM n baseOps

-- Convenience function. Converts a list into an array, starting with index 1
arrayFromList :: (Num b, Ix b) => [a] -> Array b a
arrayFromList list = listArray (1, len list) list

-- For prevention of null division
containsNullDiv :: [Int] -> OpSeq -> Bool
containsNullDiv nums ops = elem ((./.), 0) $ zip ops (tail nums)

--

eval :: Array Int Int -> Array Int ArithOp -> Int
eval nums ops = eval' (elems nums) (elems ops)

-- Evaluates the input lists. Similar to a stack, it removes the first two numbers
-- from the list and replaces them with the intermediate result.
eval' :: [Int] -> OpSeq -> Int
eval' (x:y:[]) (f:[]) = f x y 
eval' (x:y:xs) (f:fs) = eval' ((f x y):xs) fs 

--

yield :: Array Int Int -> Int -> [Array Int ArithOp]
yield = yield_bt

yield_bt :: Array Int Int -> Int -> [Array Int ArithOp]
yield_bt nums target = map arrayFromList $ yield_bt' (elems nums) target

yield_bt' :: [Int] -> Int -> [OpSeq]
yield_bt' nums target = searchDfs succ goal []
  where goal ops = isGoal ops nums target
        succ ops = nextOpseq ops nums

-- An operator sequence is a valid goal if 1) it has the correct length 
-- and 2) it yields the correct result.
isGoal :: OpSeq -> [Int] -> Int -> Bool
isGoal ops nums target = (len ops == (len nums - 1)) && eval' nums ops == target

-- If there are already enough operators, doesn't generate a new sequence.
-- If just one is missing, returns all sequences not containing a null division.
-- Else returns all sequences.
nextOpseq :: OpSeq -> [Int] -> [OpSeq]
nextOpseq ops nums
  | len ops >= neededLength = []
  | len ops == (neededLength - 1) = filter (not . containsNullDiv nums) nextSeqs
  | otherwise = nextSeqs
  where neededLength = (len nums) - 1
        nextSeqs = map (:ops) baseOps


searchDfs :: (Eq node) => (node -> [node]) -> (node -> Bool) -> node -> [node]
searchDfs succ goal x = (search' (push x emptyStack) )
  where
    search' s
      | stackEmpty s = []
      | goal (top s) = top s : search' (pop s)
      | otherwise = let x = top s in search' (foldr push (pop s) (succ x))

--

yield_gtf :: Array Int Int -> Int -> [Array Int ArithOp]
yield_gtf nums target = map arrayFromList $ yield_gtf' (elems nums) target

yield_gtf' :: [Int] -> Int -> [OpSeq]
yield_gtf' nums target = (filt target . transform nums . generate) nums

-- Generates all possible sequences not containing a null division.
generate :: [Int] -> [OpSeq]
generate nums = let allSeqs = opseqs (len nums - 1)
                in filter (not . containsNullDiv nums) allSeqs

-- Adds the result to each operator sequence.
transform :: [Int] -> [OpSeq] -> [(OpSeq, Int)]
transform nums opseqs' = map (\ops -> (ops, eval' nums ops)) opseqs'

-- Evaluates results, returns correct sequences
filt :: Int -> [(OpSeq, Int)] -> [OpSeq]
filt target = map fst . filter ((target==) . snd)

--

instance Show (Int -> Int -> Int) where
	show op = showOp op

instance Eq (Int -> Int -> Int) where
	op1 == op2 = (showOp op1) == (showOp op2)

showOp :: (Int -> Int -> Int) -> String
showOp op
	| op 5 5 == 10 = "plus"
	| op 5 5 == 0 = "minus"
	| op 5 5 == 25 = "times"
	| op 5 5 == 1 = "div"


--

data Stack a = EmptyStk | Stk a (Stack a)

push :: a -> Stack a -> Stack a
push x st = Stk x st

pop :: Stack a -> Stack a
pop (Stk _ st) = st

top :: Stack a -> a
top (Stk x _) = x

emptyStack :: Stack a
emptyStack = EmptyStk

stackEmpty :: Stack a -> Bool
stackEmpty EmptyStk = True
stackEmpty _ = False

--

{-test1 f = f (array (1,3) [(1,1),(2,2),(3,3)]) 6
test2 f = f (array (1,3) [(1,1),(2,2),(3,3)]) 4
test3 f = f (array (1,3) [(1,1),(2,2),(3,3)]) 0
test4 f = f (array (1,5) [(1,3),(2,1),(3,5),(4,2),(5,5)]) 5
testZero f = f (array (1,3) [(1,2),(2,0),(3,3)]) 6-}
