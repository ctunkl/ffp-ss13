import Data.Array
import Data.List
import Data.Ord


-- Teil 1

mas :: Array Int Int -> Int
mas = maximum . map sum . map (map snd) . segments

amas :: Array Int Int -> [(Int,Int)]
amas xs = let segs = segments xs   
              segmentBounds seg = (fst $ head seg, fst $ last seg)   
              segmentSum = sumBy snd
              results = map segmentBounds (maximaBy segmentSum segs)
          in sort results


lmas :: Array Int Int -> (Int,Int)
lmas xs = 
    let segs = amas xs        
        results = maximaBy (\(i,j) -> j-i) segs
    in head $ sort results


--

segments :: Ix i => Array i e -> [[(i,e)]]
segments = filter (not . null) . concatMap tails . inits . assocs

maximaBy :: Ord b => (a -> b) -> [a] -> [a]
maximaBy f xs = let m = f $ maximumBy (comparing f) xs
                in filter ((==m) . f) xs

sumBy :: Num b => (a -> b) -> [a] -> b
sumBy f = sum . map f


-- Teil 2

minIndex :: (Ix a, Show a) => Array a b -> (b -> Bool) -> a
minIndex arr wf = case (minIndex' arr wf) of
                    [] -> error "No matching index"
                    xs -> head xs


minIndex' arr wf = divideAndConquer mi_indiv (mi_solve wf) mi_divide mi_combine (assocs arr)

mi_indiv :: [(a,b)] -> Bool
mi_indiv xs = (length xs) <= 1


mi_solve :: (b -> Bool) -> [(a,b)] -> [a]
mi_solve _ [] = []
mi_solve wf ((i,v):_)
  | wf v = [i]
  | otherwise = []


mi_divide :: [(a,b)] -> [[(a,b)]]
mi_divide (p:pb) = [[p], pb]


mi_combine :: [(a,b)] -> [[a]] -> [a]
mi_combine p sols = concat sols

--

divideAndConquer :: (p -> Bool) -> (p -> s) -> (p -> [p]) -> (p -> [s] -> s) -> p -> s
divideAndConquer indiv solve divide combine initPb = dAC initPb
  where dAC pb
          | indiv pb = solve pb
          | otherwise = combine pb (map dAC (divide pb))


